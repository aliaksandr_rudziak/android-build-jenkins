from setuptools import setup
import sys

reload(sys)
sys.setdefaultencoding("ISO-8859-1")

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='build_jenkins_ci',
      version='0.0.1',
      description='Tools for configuring build process for continuous integration with Jenkins',
      long_description=readme(),
      classifiers=[
        'Development Status :: 1 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Topic :: Text Processing :: Linguistic',
      ],
      keywords='jenkins android git build',
      url='local',
      author='Alex Rudyak',
      author_email='aliaksandr.rudziak@instinctools.ru',
      license='MIT',
      packages=[
        'lib',
        'git_hooks',
        'jenkins_config'
      ],
      install_requires=[
        'python-jenkins'
      ],
      entry_points={
        'console_scripts': [
          'hook_in_git=git_hooks.command_line:main',
          'config_jenkins=jenkins_config.command_line:main'
        ]
      },
      include_package_data=True,
      zip_safe=False)