import subprocess

def call_command(command):
	process = subprocess.Popen(command.split(' '), 
		stdout=subprocess.PIPE,
		stdin=subprocess.PIPE,
		stderr=subprocess.PIPE)
	output,_ = process.communicate()
	return output